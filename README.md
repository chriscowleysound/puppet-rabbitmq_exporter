
# rabbitmq_exporter

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with rabbitmq_exporter](#setup)
    * [What rabbitmq_exporter affects](#what-rabbitmq_exporter-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with rabbitmq_exporter](#beginning-with-rabbitmq_exporter)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the RabbitMQ exporter for Prometheus

## Setup

### What rabbitmq_exporter affects **OPTIONAL**

Installs the binary for the exporter itself

Create a Systemd service to run it


### Setup Requirements **OPTIONAL**

TODO

### Beginning with rabbitmq_exporter  

TODO

## Usage

TODO

## Reference

TODO 

Users need a complete list of your module's classes, types, defined types providers, facts, and functions, along with the parameters for each. You can provide this list either via Puppet Strings code comments or as a complete list in the README Reference section.

* If you are using Puppet Strings code comments, this Reference section should include Strings information so that your users know how to access your documentation.

* If you are not using Puppet Strings, include a list of all of your classes, defined types, and so on, along with their parameters. Each element in this listing should include:

  * The data type, if applicable.
  * A description of what the element does.
  * Valid values, if the data type doesn't make it obvious.
  * Default value, if any.

## Limitations

Linux only

## Development

Merge Requests FTW
