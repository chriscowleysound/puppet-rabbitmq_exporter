# == Class rabbitmq_exporter::config
#
# This class is called from rabbitmq_exporter for service config.
#
class rabbitmq_exporter::config {
  file { $::rabbitmq_exporter::conf_file :
    ensure  => present,
    content => epp('rabbitmq_exporter/rabbitmq_exporter.sysconfig.epp'),
  }
}

