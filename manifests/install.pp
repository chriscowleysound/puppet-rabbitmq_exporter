# Class rabbitmq_exporter::install
# Install Prometheus RabbitMQ Exporter

class rabbitmq_exporter::install {
  include ::archive

  if $rabbitmq_exporter::manage_user {
    ensure_resource( 'user', ['rabbitmq_exporter'], {
      ensure => 'present',
      system => true,
    })
  }
  if $rabbitmq_exporter::manage_group {
    ensure_resource( 'group', [ 'rabbitmq_exporter'], {
      ensure => 'present',
      system => true,
    })
    Group['rabbitmq_exporter']->User['rabbitmq_exporter']
  }
  $package_name = $rabbitmq_exporter::package_name
  $os = $rabbitmq_exporter::os
  $arch = $rabbitmq_exporter::arch
  $archive_name = "${package_name}-${rabbitmq_exporter::version}.${os}-${arch}"
  archive { "/tmp/${archive_name}.tar.gz":
    ensure          => present,
    extract         => true,
    extract_path    => '/opt',
    source          => $rabbitmq_exporter::real_archive_url,
    checksum_verify => false,
    creates         => "/opt/${archive_name}/rabbitmq_exporter",
    cleanup         => true,
  }
  ->file {"/opt/${archive_name}/rabbitmq_exporter":
    owner => 'root',
    group => 'root',
    mode  => '0555',
  }

}
