# Class rabbitmq_exporter::service
#
# Class meant to be called from rabbitmq_installer

class rabbitmq_exporter::service {
  $package_name = $rabbitmq_exporter::package_name
  $os = $rabbitmq_exporter::os
  $arch = $rabbitmq_exporter::arch
  $archive_name = "${package_name}-${rabbitmq_exporter::version}.${os}-${arch}"

  include ::systemd
  file {'/etc/systemd/system/rabbitmq_exporter.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('rabbitmq_exporter/rabbitmq_exporter.systemd.epp', {
        'user'      => 'rabbitmq_exporter',
        'group'     => 'rabbitmq',
        'conf_file' => $::rabbitmq_exporter::conf_file,
        'bin_dir'   => "/opt/${archive_name}"
      },
    )
  }
  ~>Exec['systemctl-daemon-reload']

  service { 'rabbitmq_exporter':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }
}
