# Class rabbitmq_exporter

#
# Installs RabbitMQ exporter for Prometheus

class rabbitmq_exporter (
  $add_to_consul = false,
  $version       = $::rabbitmq_exporter::params::version,
  $archive_url_base = $::rabbitmq_exporter::params::archive_url_base,
  $archive_url      = $::rabbitmq_exporter::params::archive_url,
  $real_archive_url = $::rabbitmq_exporter::params::real_archive_url,
  $package_name     = $::rabbitmq_exporter::params::package_name,
  $arch             = $::rabbitmq_exporter::params::arch,
  $os               = $::rabbitmq_exporter::params::os,
  $manage_service   = $::rabbitmq_exporter::params::manage_service,
  $manage_user      = $::rabbitmq_exporter::params::manage_user,
  $manage_group     = $::rabbitmq_exporter::params::manage_group,
  $conf_file        = $::rabbitmq_exporter::params::conf_file,
  $manager_consul   = $::rabbitmq_exporter::params::manage_consul,
) inherits rabbitmq_exporter::params {
  # validation
  class {'::rabbitmq_exporter::install': }
  ->class { '::rabbitmq_exporter::config': }
  ~>class { '::rabbitmq_exporter::service': }
  ->Class['::rabbitmq_exporter']
}
