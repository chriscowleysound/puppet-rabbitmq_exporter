# Class rabbitmq_exporter::params
# Default params for rabbitmq_exporter class

class rabbitmq_exporter::params {
  $version           = '0.20.0'
  $archive_url_base  = 'https://github.com/kbudde/rabbitmq_exporter/releases/download'
  $package_name      = 'rabbitmq_exporter'
  $archive_url       = undef
  $manage_service    = true
  $manage_user       = true
  $manage_group     = true
  $manage_consul     = false

  case $::architecture {
    'x86_64', 'amd64': { $arch = 'amd64' }
    'i386':            { $arch = '386' }
    default:           {
      fail("Unsupported kernel architecture: ${::architecture}")
    }
  }
  case $::kernel {
    'Linux': { $os = downcase($::kernel)}
    default: {
      fail("Unsupported OS ${::kernel}")
    }
  }
  case $::osfamily {
    'Debian': { $conf_file = '/etc/default/rabbitmq_exporter' }
    'RedHat': { $conf_file = '/etc/sysconfig/rabbitmq_exporter' }
    default: {
      fail("${::operatingsystem} not supported")
    }
  }
  $real_archive_url = pick(
    $archive_url,
    "${archive_url_base}/v${version}/${package_name}-${version}.${os}-${arch}.tar.gz"
  )
}
