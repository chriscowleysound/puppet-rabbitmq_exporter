require 'spec_helper'

describe 'rabbitmq_exporter' do
  context 'supported os' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts
        end

        context 'rabbitmq_exporter class with no parameters' do
          it { is_expected.to compile.with_all_deps }
        end
      end
    end
  end
  context 'unsupported os' do
    describe 'rabbitmq_exporter class without any parameterss on Solaris/Nexenta' do
      let(:facts) do
        {
          osfamily: 'Solaris',
          operatingsystem: 'Nexenta',
        }
      end

      it { expect { is_expected.to contain_package('rabbitmq_exporter') }.to raise_error(Puppet::Error, %r{not supported}) }
    end
  end
end
